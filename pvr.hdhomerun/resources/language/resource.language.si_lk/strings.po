# Kodi Media Center language file
# Addon Name: PVR HDHomeRun Client
# Addon id: pvr.hdhomerun
# Addon Provider: Zoltan Csizmadia (zcsizmadia@gmail.com)
msgid ""
msgstr ""
"Project-Id-Version: KODI Main\n"
"Report-Msgid-Bugs-To: translations@kodi.tv\n"
"POT-Creation-Date: YEAR-MO-DA HO:MI+ZONE\n"
"PO-Revision-Date: 2025-01-17 23:11+0000\n"
"Last-Translator: Anonymous <noreply@weblate.org>\n"
"Language-Team: Sinhala (Sri Lanka) <https://kodi.weblate.cloud/projects/kodi-add-ons-pvr-clients/pvr-hdhomerun/si_lk/>\n"
"Language: si_lk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Weblate 5.9.2\n"

msgctxt "Addon Summary"
msgid "HDHomeRun PVR Client"
msgstr ""

msgctxt "Addon Description"
msgid "HDHomeRun PVR Client"
msgstr ""

msgctxt "#32001"
msgid "General"
msgstr "සාමාන්‍ය"

msgctxt "#32002"
msgid "Hide protected channels"
msgstr ""

msgctxt "#32003"
msgid "Enable debug logging"
msgstr ""

msgctxt "#32004"
msgid "Hide duplicate channels"
msgstr ""

msgctxt "#32005"
msgid "Mark new show"
msgstr ""

msgctxt "#32006"
msgid "Use HTTP discovery"
msgstr ""
