# Kodi Media Center language file
# Addon Name: PVR HDHomeRun Client
# Addon id: pvr.hdhomerun
# Addon Provider: Zoltan Csizmadia (zcsizmadia@gmail.com)
msgid ""
msgstr ""
"Project-Id-Version: KODI Main\n"
"Report-Msgid-Bugs-To: translations@kodi.tv\n"
"POT-Creation-Date: YEAR-MO-DA HO:MI+ZONE\n"
"PO-Revision-Date: 2025-01-17 23:11+0000\n"
"Last-Translator: Anonymous <noreply@weblate.org>\n"
"Language-Team: English (New Zealand) <https://kodi.weblate.cloud/projects/kodi-add-ons-pvr-clients/pvr-hdhomerun/en_nz/>\n"
"Language: en_nz\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.9.2\n"

msgctxt "Addon Summary"
msgid "HDHomeRun PVR Client"
msgstr "HDHomeRun PVR Client"

msgctxt "Addon Description"
msgid "HDHomeRun PVR Client"
msgstr "HDHomeRun PVR Client"

msgctxt "#32001"
msgid "General"
msgstr "General"

msgctxt "#32002"
msgid "Hide protected channels"
msgstr "Hide protected channels"

msgctxt "#32003"
msgid "Enable debug logging"
msgstr "Enable debug logging"

msgctxt "#32004"
msgid "Hide duplicate channels"
msgstr "Hide duplicate channels"

msgctxt "#32005"
msgid "Mark new show"
msgstr "Mark new show"

msgctxt "#32006"
msgid "Use HTTP discovery"
msgstr ""
