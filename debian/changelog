kodi-pvr-hdhomerun (21.0.2+ds-1) unstable; urgency=high

  * New upstream version 21.0.2+ds
  * d/copyright: Bump copyright years

 -- Vasyl Gello <vasek.gello@gmail.com>  Mon, 27 Jan 2025 08:45:48 +0000

kodi-pvr-hdhomerun (21.0.1+ds-2) unstable; urgency=critical

  * Mass fix: d/watch: Allow multiple addon suites to be checked

 -- Vasyl Gello <vasek.gello@gmail.com>  Tue, 20 Aug 2024 12:13:35 +0000

kodi-pvr-hdhomerun (21.0.1+ds-1) unstable; urgency=medium

  * New upstream version 21.0.1+ds

 -- Vasyl Gello <vasek.gello@gmail.com>  Fri, 09 Aug 2024 07:57:07 +0000

kodi-pvr-hdhomerun (21.0.0+ds-1) experimental; urgency=medium

  * New upstream version 21.0.0+ds
  * Branch out experimental
  * Modernize package
  * New release 20.3.0+ds1-1
  * Finalize changelog
  * d/gbp.conf: Fix debian and upstream branches
  * Import Debian packaging from debian/sid
  * Prepare for v21 "Omega"
  * Bump copyright years
  * d/control: Ensure Vcs-Git points to correct branch

 -- Vasyl Gello <vasek.gello@gmail.com>  Sat, 09 Mar 2024 15:09:56 +0000

kodi-pvr-hdhomerun (20.4.0+ds1-1) unstable; urgency=medium

  * New upstream version 20.4.0+ds1
  * d/watch: switch to git tags

 -- Vasyl Gello <vasek.gello@gmail.com>  Sat, 15 Oct 2022 16:53:41 +0000

kodi-pvr-hdhomerun (20.3.0+ds1-1) unstable; urgency=medium

  * New upstream version 20.3.0+ds1
  * Prepare for v20 in unstable

 -- Vasyl Gello <vasek.gello@gmail.com>  Thu, 04 Aug 2022 09:53:43 +0000

kodi-pvr-hdhomerun (19.1.0+ds1-1) unstable; urgency=medium

  * New upstream version 19.1.0+ds1
  * Modernize package

 -- Vasyl Gello <vasek.gello@gmail.com>  Mon, 21 Mar 2022 17:46:58 +0000

kodi-pvr-hdhomerun (19.0.0+ds1-1) unstable; urgency=medium

  * New upstream version 19.0.0+ds1
  * Bump standard version
  * Disable LTO to make build reproducible

 -- Vasyl Gello <vasek.gello@gmail.com>  Wed, 13 Oct 2021 22:13:46 +0000

kodi-pvr-hdhomerun (7.1.1+ds1-1) unstable; urgency=medium

  * New upstream version 7.1.1+ds1
  * Fix github ref
  * Restrict watchfile to current stable Kodi codename

 -- Vasyl Gello <vasek.gello@gmail.com>  Sun, 15 Aug 2021 21:31:37 +0000

kodi-pvr-hdhomerun (7.1.0+ds1-1) unstable; urgency=medium

  * New upstream version 7.1.0+ds1
  * Force override libdir paths
  * Bump copyright years

 -- Vasyl Gello <vasek.gello@gmail.com>  Wed, 20 Jan 2021 06:23:56 +0000

kodi-pvr-hdhomerun (7.0.0+ds1-1) unstable; urgency=medium

  * New upstream version 7.0.0+ds1
    + Repack out vendored dependencies
  * d/rules:
    + Use architecture.mk for DEB_HOST_MULTIARCH
    + Enable hardening flags
  * Update d/watch
  * d/control:
    + Remove Balint Reczey from Uploaders per his wish, and add myself
    + Use dh-sequence-kodiaddon
    + Drop kodiplatform build-dependency
    + Update Vcs-* for the move to 'kodi-media-center' subgroup
    + Bump Standards-Version to 4.5.1, no changes needed
    + Bump debhelper compat level to 13, use debhelper-compat and drop d/compat
  * Update d/copyright
  * Add new d/upstream/metadata

 -- Vasyl Gello <vasek.gello@gmail.com>  Sun, 20 Dec 2020 22:15:05 +0000

kodi-pvr-hdhomerun (3.4.3-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/control: Set Vcs-* to salsa.debian.org

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ Balint Reczey ]
  * Add debian/watch file
  * Add basic Salsa CI configuration
  * New upstream version 3.4.3
  * Bump libkodiplatform-dev and kodi-addons-dev build-dependency versions
  * Refresh patches
  * Build-depend on kodi-addons-dev (>= 2:18~) (with correct epoch)

 -- Balint Reczey <rbalint@ubuntu.com>  Sat, 21 Mar 2020 16:38:25 +0100

kodi-pvr-hdhomerun (2.4.2+git20160820-2) unstable; urgency=medium

  * Secure Vcs-Git URL
  * Adapt to Kodi PVR api change - SeekTime (Closes: #849230)
  * Depend on PVR API version of Kodi we built the addon with

 -- Balint Reczey <balint@balintreczey.hu>  Sat, 24 Dec 2016 02:56:04 +0100

kodi-pvr-hdhomerun (2.4.2+git20160820-1) unstable; urgency=medium

  * Imported Upstream version 2.4.2+git20160820
  * Build-depend on latest libkodiplatform-dev and kodi-addons-dev
  * Build-depend on kodi-addons-dev version which has all the needed headers
  * Drop obsoleted patches
  * Make debian/rules executable
  * Drop hack to build with p8-platform

 -- Balint Reczey <balint@balintreczey.hu>  Thu, 03 Nov 2016 22:39:43 +0100

kodi-pvr-hdhomerun (1.0.12+git20160316-1) unstable; urgency=medium

  * New upstream version 1.0.12+git20160316
  * Refresh patches
  * Extend package description
  * Tidy up debian/control
  * Bump policy version to 3.9.8

 -- Balint Reczey <balint@balintreczey.hu>  Sat, 16 Apr 2016 00:10:42 +0200

kodi-pvr-hdhomerun (1.0.4+git20151120-1) unstable; urgency=medium

  * Fix build by fixing C++ type using upstream patch
  * Create packaging compliant to Debian Policy

 -- Balint Reczey <balint@balintreczey.hu>  Fri, 18 Dec 2015 01:35:17 +0100
