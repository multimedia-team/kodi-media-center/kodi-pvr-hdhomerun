# Kodi Media Center language file
# Addon Name: PVR HDHomeRun Client
# Addon id: pvr.hdhomerun
# Addon Provider: Zoltan Csizmadia (zcsizmadia@gmail.com)
msgid ""
msgstr ""
"Project-Id-Version: KODI Main\n"
"Report-Msgid-Bugs-To: translations@kodi.tv\n"
"POT-Creation-Date: YEAR-MO-DA HO:MI+ZONE\n"
"PO-Revision-Date: 2025-01-17 23:11+0000\n"
"Last-Translator: Massimo Pissarello <mapi68@gmail.com>\n"
"Language-Team: Italian <https://kodi.weblate.cloud/projects/kodi-add-ons-pvr-clients/pvr-hdhomerun/it_it/>\n"
"Language: it_it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.9.2\n"

msgctxt "Addon Summary"
msgid "HDHomeRun PVR Client"
msgstr "Client PVR HDHomeRun"

msgctxt "Addon Description"
msgid "HDHomeRun PVR Client"
msgstr "Client PVR HDHomeRun"

msgctxt "#32001"
msgid "General"
msgstr "Generale"

msgctxt "#32002"
msgid "Hide protected channels"
msgstr "Nascondi canali protetti"

msgctxt "#32003"
msgid "Enable debug logging"
msgstr "Abilita la registrazione di debug"

msgctxt "#32004"
msgid "Hide duplicate channels"
msgstr "Nascondi i canali duplicati"

msgctxt "#32005"
msgid "Mark new show"
msgstr "Contrassegna nuovo spettacolo"

msgctxt "#32006"
msgid "Use HTTP discovery"
msgstr "Usa il rilevamento HTTP"
